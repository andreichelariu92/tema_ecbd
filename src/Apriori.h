#ifndef Apriori_H_INCLUDE_GUARD
#define Apriori_H_INCLUDE_GUARD

#include <vector>
#include <string>
#include <map>

typedef std::vector<std::vector<int> > Matrix_t;
typedef std::vector<int> ItemSet_t;

///Structure that holds a rule
//determined based on an item set.
///@left holds a vector of items
///@right holds an item that has
///high probability of occurrence
///with the items in the left vector.
struct Rule
{
    std::vector<int> left;
    int right;

    Rule(ItemSet_t argLeft, int argRight)
        :left(argLeft),
         right(argRight)
    {}
};

///Function that implements the apriori algorithm.
///Returns a vector of rules based on the
///item sets and the two parameters.
std::vector<Rule> apriori(std::vector<int> items, 
                          Matrix_t itemSets, 
                          double minProbability, 
                          double minConfidence);

///Helper functions

///Returns true if all the elements
///in v1 are also in v2. The order
///does not matter.
bool isIncluded(std::vector<int> v1,
                       std::vector<int> v2);

///Computes the probability of
///an item to be found in an item set
double probability(int item,
                   Matrix_t itemSets);

///Computes the probability of the
///secquence of items to be found
///in the item sets.
double probability(std::vector<int> itemSet,
                   Matrix_t itemSets);

///Function that returns a vector
///containing all the combinations
///that can be formed from the given
///input vector.
Matrix_t combinations(std::vector<int> items, int size);

///Returns a vector of rules
///that can be deduced from the given
///item set and minimum confidence
std::vector<Rule> makeRules(std::vector<int> itemSet,
                            std::map<ItemSet_t, double>& mapItemSet,
                            double minConfidence);

///Returns the first element that is in itemSet1
///and it is not in itemSet2
int difference(ItemSet_t itemSet1, ItemSet_t itemSet2);

///After this function is called, rules1
///will contain the elements from rule2
///at the end.
void appendVector(std::vector<Rule>& rules1, 
                  std::vector<Rule>& rules2);

///Reads the input from the file and
///sets the output parameters
void readInput(std::string fileName, 
               std::vector<int>& items,
               Matrix_t& itemSets);
#endif
