#include <algorithm>
#include <iostream>
#include <map>
#include <fstream>

#include "Apriori.h"

using std::vector;
using std::cout;
using std::map;
using std::string;
using std::fstream;

bool isIncluded(vector<int> v1,
                vector<int> v2)
{
    for (int item : v1)
    {
        vector<int>::iterator position;
        position = std::find(v2.begin(), v2.end(), item);
        if (position == v2.end())
        {
            return false;
        }
    }

    return true;
}

double probability(int item,
                   Matrix_t itemSets)
{
    vector<int> tempVector(1, item);
    return probability(tempVector, itemSets);
}
double probability(vector<int> itemSet,
                   Matrix_t itemSets)
{
    double count = 0.0;
    
    for (vector<int>& set : itemSets)
    {
        if (isIncluded(itemSet, set))
        {
            count++;
        }
    }

    return (count / itemSets.size());

}

vector<Rule> apriori(vector<int> items,
                     Matrix_t itemSets,
                     double minProbability,
                     double minConfidence)
{
    vector<Rule> output;
    map<vector<int>, double> mapItemSets;

    //determine the items that have
    //probability > minProbability
    vector<int> probableItems;
    for (int item : items)
    {
        if (probability(item, itemSets)
            >
            minProbability)
        {
            probableItems.push_back(item);
        }
    }
    
    //determine item sets of increasing
    //size that have probability >
    //minProbability
    unsigned int itemSetSize = 1;
    unsigned int addedItemSets = 0;
    do
    {
        ++itemSetSize;

        addedItemSets = 0;
        Matrix_t itemCombinations = 
            combinations(probableItems, itemSetSize);
        
        for (vector<int>& itemCombination : itemCombinations)
        {
            const double p = probability(itemCombination, itemSets);
            if (p > minProbability)
            {
                ++addedItemSets;
                mapItemSets[itemCombination] = p;
            }
        }

    }while(addedItemSets > 0);
    
    //get the rules for all
    //the item sets with more
    //than 3 items
    typedef map<vector<int>, double>::iterator MapIt_t;
    for (MapIt_t mapIt = mapItemSets.begin();
         mapIt != mapItemSets.end();
         ++mapIt)
    {
        if ((mapIt->first).size() >= 3)
        {
            vector<Rule> rules = makeRules(mapIt->first,
                                           mapItemSets,
                                           minConfidence);
            appendVector(output, rules);
        }
    }

    return output;
}

Matrix_t combinations(vector<int> items, int size)
{
    //INSPIRATION:
    //http://stackoverflow.com/questions/12991758/creating-all-possible-k-combinations-of-n-items-in-c
    
    Matrix_t output;
    
    if (items.size() < (unsigned)size)
    {
        return output;
    }

    std::string bitmask(size, 1);
    bitmask.resize(items.size(), 0);
    do
    {
        vector<int> partialVector;

        unsigned int i;
        for (i = 0; i < items.size(); ++i)
        {
            if (bitmask[i])
            {
                partialVector.push_back(items[i]);
            }
        }

        output.push_back(partialVector);
    }while(std::prev_permutation(bitmask.begin(), bitmask.end()));

    return output;
}

int difference(ItemSet_t itemSet1, ItemSet_t itemSet2)
{
    for (int item : itemSet1)
    {
        if (std::find(itemSet2.begin(), itemSet2.end(), item)
            ==
            itemSet2.end())
        {
            return item;
        }
    }

    return -1;
}
vector<Rule> makeRules(vector<int> itemSet,
                       map<ItemSet_t, double>& mapItemSet,
                       double minConfidence)
{
    vector<Rule> output;
    const double itemSetProbability = mapItemSet[itemSet];
    
    //add subsets of itemSet which
    //have confidence higher than
    //minConfidence
    const unsigned int leftRuleSize = itemSet.size() - 1;
    Matrix_t subItemSets = combinations(itemSet, leftRuleSize);

    for (ItemSet_t subItemSet : subItemSets)
    {
        const double confidence = itemSetProbability
                                  /
                                  mapItemSet[subItemSet];

        if (confidence > minConfidence)
        {
            Rule r(subItemSet, difference(itemSet, subItemSet));
            output.push_back(r);
        }
    }

    return output;
}

void appendVector(vector<Rule>& rules1, vector<Rule>& rules2)
{
    for (Rule& rule : rules2)
    {
        rules1.push_back(rule);
    }
}

void readInput(string fileName, 
               vector<int>& items,
               Matrix_t& itemSets)
{
    std::ifstream file(fileName, std::ios_base::in);

    //read the vector of items
    unsigned int nrItems;
    file >> nrItems;
    items.resize(nrItems);
    unsigned int itemIdx;
    for (itemIdx = 0;
         itemIdx < nrItems;
         ++itemIdx)
    {
        file >> items[itemIdx];
    }

    //read the item sets
    unsigned int nrItemSets;
    file >> nrItemSets;
    itemSets.resize(nrItemSets);
    
    //read every item set
    unsigned int itemSetIdx;
    for (itemSetIdx = 0;
         itemSetIdx < nrItemSets;
         ++itemSetIdx)
    {
        unsigned int itemSetSize;
        file >> itemSetSize;
        itemSets[itemSetIdx].resize(itemSetSize);

        for (unsigned int i = 0;
             i < itemSetSize;
             ++i)
        {
            file >> itemSets[itemSetIdx][i];
        }
    }
}

