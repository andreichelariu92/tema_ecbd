#include <iostream>

#include "Apriori.h"

using std::vector;
using std::cout;

int main()
{
    Matrix_t itemSets1;
    vector<int> items1;
    readInput("./input.txt", items1, itemSets1);
    vector<Rule> rules = apriori(items1, itemSets1, 0.4, 0.5);

    for (Rule& rule : rules)
    {
        for (auto element : rule.left)
        {
            cout << element << "^";
        }

        cout << "->" << rule.right << "\n";
    }
    return 0;
}
